plugins {
    id("java")
    id("war")
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
}

group = "ru.tulinov"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}


dependencies {
    implementation("org.springframework.kafka:spring-kafka:2.7.6")
    implementation("org.springframework:spring-beans:5.3.9")
    implementation("org.springframework:spring-core:5.3.9")
    implementation("org.springframework:spring-context:5.3.9")
    implementation("org.springframework:spring-web:5.3.9")
    implementation("org.springframework:spring-webmvc:5.3.9")
    implementation("javax.servlet:javax.servlet-api:4.0.1")

    implementation("org.slf4j:slf4j-api:1.7.26")
    implementation ("org.slf4j:slf4j-simple:1.7.26")

    implementation ("org.jboss.logging:jboss-logging:3.4.3.Final")
    implementation ("org.projectlombok:lombok:1.18.32")
    implementation ("com.fasterxml.jackson.core:jackson-databind:2.17.0")

    annotationProcessor ("org.projectlombok:lombok:1.18.32")

    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")


}



tasks.test {
    useJUnitPlatform()
}