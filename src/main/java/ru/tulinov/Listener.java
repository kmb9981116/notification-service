package ru.tulinov;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.tulinov")
public class Listener {
    public static void main(String[] args) {
    }

}
