package ru.tulinov.listeners;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import ru.tulinov.event.ClaimCreatedEvent;

@Component
@Slf4j
public class KafkaListeners {
    @KafkaListener(topics = "notification-topic", groupId = "notificationId")
    public void notify(ClaimCreatedEvent claimCreatedEvent) {
        log.info("Received message: " + claimCreatedEvent.getClaimNumber());
        // Отправка уведомления на почту...
    }
}
